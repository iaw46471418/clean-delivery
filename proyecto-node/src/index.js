const express = require('express');
const path = require('path');
const morgan = require('morgan');
const passport = require('passport');
const session = require('express-session');
const app = express();

//CONEXIO BBDD
const mongoose = require('mongoose');

mongoose.connect('mongodb://localhost/proyecto_db', {
    useCreateIndex: true,
    useNewUrlParser: true,
    useFindAndModify: false,
    useUnifiedTopology: true 
})
    .then(db => console.log('db is connected'))
    .catch(err => console.log(err));

// CONFIGURACIó SERVIDOR
app.set('port', process.env.PORT || 3000);
app.set('view engine', 'ejs');
app.set('views', path.join(__dirname, 'views'));
app.use(express.urlencoded({extended: false})); // Per poder llegir dades del formulari
app.use(session({
    secret: 'mysecret',
    resave: true,
    saveUninitialized: true
}));

require('./config/passport');

// INi PASSPORT AUTHENTICATION
app.use(passport.initialize());

// INI PERSISTENT LOGIN SESSIONS
app.use(passport.session());

// CONEXIO PORT
app.use(morgan('dev'));
app.listen(app.get('port'), () => {
    console.log('Server on port', app.get('port'));
})

// IMPORTAR RUTAS
const  customerRoutes = require('./routes/customer');
const  deliveriesRoutes = require('./routes/deliveries');
const  loginRoutes = require('./routes/login');
const  newDeliveriesRoutes = require('./routes/newDelivery');

// RUTAS
app.use('/', customerRoutes);
app.use('/deliveries', deliveriesRoutes);
app.use('/login', loginRoutes);
app.use('/newDelivery', newDeliveriesRoutes);

// STATIC TEXT
app.use(express.static(path.join(__dirname, 'public')));