// CREAR I EXPORTAR RUTAS
const express = require('express');
const router = express.Router();

// Asegurar rutes
const { isAthenticated } = require('../config/auth');

//Importem els metodes de controllers
const deliveriesController = require('../controllers/deliveriesController');
router.get('/', isAthenticated, deliveriesController.list);

router.post('/deliveryDeleted', deliveriesController.removeDelivery);

module.exports = router;