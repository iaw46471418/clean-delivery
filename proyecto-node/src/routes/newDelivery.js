// CREAR I EXPORTAR RUTAS
const express = require('express');
const router = express.Router();

// Asegurar rutes
const { isAthenticated } = require('../config/auth');

//Importem els metodes de controllers
const newDeliveryController = require('../controllers/newDeliveryController');

router.get('/', isAthenticated, newDeliveryController.list);
router.post('/addDelivery', newDeliveryController.addDelivery);

router.get('/addNewMenu', newDeliveryController.addNewMenu);
router.post('/', newDeliveryController.saveNewMenu);

module.exports = router;