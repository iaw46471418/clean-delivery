// CREAR I EXPORTAR RUTAS
const express = require('express');
const router = express.Router();

// Asegurar rutes
const { isAthenticated } = require('../config/auth');

//Importem els metodes de controllers
const loginController = require('../controllers/loginController');

router.get('/', isAthenticated, loginController.list);
router.post('/new-employer', loginController.saveEmployer);

router.get('/addnewemployer', loginController.addNewEmployer);
router.post('/', loginController.saveNewEmployer);
module.exports = router;