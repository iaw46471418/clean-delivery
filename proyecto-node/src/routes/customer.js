// CREAR I EXPORTAR RUTAS
const express = require('express');
const router = express.Router();
const passport = require('passport');

//Importem els metodes de controllers
const customerController = require('../controllers/customerController');

router.get('/', (req,res) => {
    res.redirect('/signIn');
});

router.get('/signIn', customerController.signIn);

router.post('/signIn', passport.authenticate('local', {
    successRedirect: '/login',
    failureRedirect: '/signin',
}));


router.get('/signUp', customerController.signUp);

router.post('/signUp', customerController.formSignUp);

router.get('/logout', (req,res) => {
    req.logout();
    res.redirect('/signin');
});

module.exports = router;