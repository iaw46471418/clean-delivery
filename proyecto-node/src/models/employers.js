const mongoose = require('mongoose');
const { Schema } = mongoose;

const employersSchema = new Schema({
    dni: {type: Number, required:true },
    name: {type: String, required:true },
    user: {type: String, required: false },
    active: {type: Boolean, required:false }
});

module.exports = mongoose.model('employers', employersSchema);