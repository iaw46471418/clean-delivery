const mongoose = require('mongoose');
const { Schema } = mongoose;

const deliverySchema = new Schema({
    idComanda: {type: Number, required:true},
    telefonClient: {type: Number, required:true},
    adreca: {type: String, required:true},
    menu: {type: String, required:true},
    price: {type: Number, required: true},
    act: {type: Boolean, require:true},
    user: {type: String, required: true},
    obsv: {type: String, required:false}
});

module.exports = mongoose.model('deliveries', deliverySchema);