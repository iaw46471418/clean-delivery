const mongoose = require('mongoose');
const { Schema } = mongoose;

const clientSchema = new Schema({
    telefonClient: {type: Number, required:true},
    adreca: {type: String, required:true}
});

module.exports = mongoose.model('client', clientSchema);