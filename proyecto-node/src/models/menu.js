const mongoose = require('mongoose');
const { Schema } = mongoose;

const menusSchema = new Schema({
    menu: {type: String, required:true},
    plat1: {type: String, required:true},
    plat2: {type: String, required:true},
    price: {type: Number, required: true},
    user: {type: String, required: true},
});

module.exports = mongoose.model('menus', menusSchema);