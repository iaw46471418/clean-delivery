const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;

const User = require('../models/user');

// AUTENTIFICACIO D'USUARIS
passport.use(new LocalStrategy({
    usernameField: 'user'
}, async (user, password, done) => {
    const userFind = await User.findOne({user: user});
    if (userFind != null) {
        if (password == userFind.password) {
            return done(null, userFind);
        }         
        return done(null, false, { msg: 'Password incorrecte' });
    }        
    return done(null, false, { msg: 'Usuari no registrat' });
}));

// EMMAGETZAMEN D'USUARIS A UNA SESSIO
passport.serializeUser((user, done) => {
    done(null, user.id);
});

passport.deserializeUser((id, done) => {
    User.findById(id, (err, user) => {
        done(err, user);
    });
});