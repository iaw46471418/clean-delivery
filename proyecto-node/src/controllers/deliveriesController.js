const controller = {};
// IMPORTEM ESQUEMA SCHEMA BBDD
const employers = require('../models/employers'); 
const delivery = require('../models/deliveries'); 

// Cridem a la vista
controller.list = async (req, res) => {
    const data = await employers.find({active: true});
    const deliveries = await delivery.find({act: true});
    res.render('deliveries', {
        data: data,
        user: req.user.user,
        del: deliveries
    });
};

controller.removeDelivery = async (req, res) => {
    const data = await employers.find({active: true});
    const deletedId = parseInt(req.body.deleteId);

    await delivery.findOneAndUpdate({ idComanda: deletedId, user: req.user.user }, { act: false });
    deliveries = await delivery.find({act: true});
    res.render('deliveries', {
        data: data,
        user: req.user.user,
        del: deliveries
    });
};

module.exports = controller;