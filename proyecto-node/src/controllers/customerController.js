const controller = {};
// IMPORTEM SCHEMA BBDD
const User = require('../models/user'); 

// -------------- SIGN IN ----------------
controller.signIn = (req, res) => {
    res.render('signIn');
};

// -------------- SIGN UP ----------------
controller.signUp = (req, res) => {
    res.render('signUp');
};

controller.formSignUp = async (req, res) => {
    const { user, password, confirmPassword } = req.body;
    const errors = [];

    if (user <= 0) {
        errors.push({text: 'Introdueix un usuari'});
    }

    if (password != confirmPassword) {
        errors.push({text: 'Les contrasenyas no coincideixen'});
    }

    if (password.length < 4) {
        errors.push({text: 'La contrasenya ha de tindre mes de 4 caracters'});
    }

    if (errors.length > 0) {
        res.render('signUp', {errors});
    } else {
        // Busquem dins la bbdd que no es repeteixi l'usuari
        const repitedUser = await User.find({user: user});
        if (repitedUser.length != 0) {
            errors.push({text: 'El usuari introduit ya existeix'});
            res.render('signUp', {errors});
        } else {
            // Guardem nou usuari a la BBDD
            const newUser = new User({user, password});
            await newUser.save();
            res.redirect('signin');
        }
    }
};

module.exports = controller;