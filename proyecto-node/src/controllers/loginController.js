const controller = {};

// IMPORTEM ESQUEMA SCHEMA BBDD
const employers = require('../models/employers'); 

// Cridem a la vista
controller.list = async (req, res) => {
    const data = await employers.find({active: true});
    res.render('login', {
        data: data,
        user: req.user.user
    });
};

// EMPLEAT ENTRA AL SISTEMA
controller.saveEmployer = async (req,res) => {
    const id = req.body.id;
    const found = await employers.find({dni: id});
    if (found.length > 0) {
        // Actualitzar camp per actiu o inactiu
        if (found[0].active) {
            await employers.findOneAndUpdate({ dni: id }, { active: false });
        } else {
            await employers.findOneAndUpdate({ dni: id }, { active: true });
        }
        const data = await employers.find({active: true});
        res.render('login', {
            data: data,
            user: req.user.user
        });
    } else {
        const data = await employers.find({active: true});
        res.render('login', {
            errors: {text: 'Contrasenya incorrecte'},
            data: data,
            user: req.user.user
        });
    }
};

// AFEGIR NOU EMPLEAT
controller.addNewEmployer = async (req, res) => {
    const data = await employers.find({active: true});
    res.render('addNewEmployer', {
        data: data,
        user: req.user.user
    });
}

controller.saveNewEmployer = async (req, res) => {
    const dni = req.body.dni;
    const name = req.body.name;
    const user = req.user.user;
    const active = false;
    const data = await employers.find({active: true});

    if (dni == '' || name == '') {
        res.render('addNewEmployer', {
            errors: {text: 'Completa el formulari de registre'},
            data: data,
            user: req.user.user
        });
    } else {
        // Busquem si ya existeicx l'usuari
        const repitedUser = await employers.find({dni: dni});
        if (repitedUser.length == 0) {
            const newEmployer = new employers({dni, name, user, active});
            await newEmployer.save();
            res.redirect('/login');
        } else {
            res.render('addNewEmployer', {
                errors: {text: 'Aquest dni ya esta registrat'},
                data: data,
                user: req.user.user
            });
        }
    }
}

module.exports = controller;