const controller = {};

// IMPORTEM ESQUEMA SCHEMA BBDD
const employers = require('../models/employers'); 
const menus = require('../models/menu'); 
const delivery = require('../models/deliveries');
const client = require('../models/client');

// Cridem a la vista
controller.list = async (req, res) => {
    const data = await employers.find({active: true});
    const menu = await menus.find();
    res.render('newDelivery', {
        data: data,
        user: req.user.user,
        menu: menu
    });
};

// AFEGIR COMANDA
controller.addDelivery = async (req, res) => {
    const del = require('../models/deliveries');

    const telefonClient = parseInt(req.body.phone);
    const adreca = req.body.street;
    const menu = req.body.menu;
    const obsv = req.body.obs; 
    const data = await employers.find({active: true});
    const menuG = await del.find();
    const act = true;
    const id = await del.find({user: req.user.user});
    const idComanda = id.length;
    const user = req.user.user;
    console.log(user, idComanda);

    if (telefonClient == '' || adreca == '' || menu == '') {
        res.render('newDelivery', {
            errors: {text: 'Completa el formulari'},
            data: data,
            user: req.user.user,
            menu: menuG
        });
    } else {
        const pricem = await menus.findOne({menu: menu});
        const price = pricem.price;
        const addDelivery = new delivery({idComanda, telefonClient, adreca, menu, price, act, user, obsv});
        await addDelivery.save();
        // AFEGIM CLIENT
        const clientAntic = await client.find({telefonClient: telefonClient});
        if ( clientAntic.length == 0 ) {
            const addDelivery = new client({telefonClient, adreca});
            await addDelivery.save();
        }

        const deliveries = await delivery.find({act: true});
        res.render('deliveries', {
            data: data,
            user: req.user.user,
            del: deliveries
        });
    }
};

//AFEGIR NOU MENU
controller.addNewMenu = async (req, res) => {
    const data = await employers.find({active: true});
    res.render('addNewMenu', {
        data: data,
        user: req.user.user
    });
};

controller.saveNewMenu = async (req, res) => {
    const menu = req.body.menu;
    const plat1 = req.body.plat1;
    const plat2 = req.body.plat2;
    const price = req.body.price;
    const data = await employers.find({active: true});
    const user = req.user.user;

    if (menu == '' || plat2 == '' || plat1 == '' || price == '') {
        res.render('addNewMenu', {
            errors: {text: 'Completa el formulari'},
            data: data,
            user: req.user.user
        });
    } else {
        const repetit = await menus.find({menu: menu, user: user});
        if(repetit.length == 0) {
            const addMenu = new menus({menu, plat1, plat2, price, user});
            await addMenu.save();
            res.redirect('/newDelivery');
        } else {
            res.render('addNewMenu', {
                errors: {text: 'Aquest nom del menu ya existeix'},
                data: data,
                user: req.user.user
            });
        }
    }
};
module.exports = controller;