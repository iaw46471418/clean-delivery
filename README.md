# Clean Delivery

### Instalació NODEJS/NVM:

$ curl -o- https://raw.githubusercontent.com/creationix/nvm/v0.33.11/install.sh | bash  
$ command -v nvm  
$ nvm ls-remote  
$ nvm install node  

Enllaç on instalar node: https://comoinstalar.info/nodejs-en-linux/
(Recomenem instalar node amb nvm per tindre'l actualitzat)

### Instalació MongoDB:

$ sudo vi /etc/yum.repos.d/mongodb.repo
Copiar dins de mongodb.repo:	
--------------------------------------------------------------------------------------
		[Mongodb]
		name=MongoDB Repository
		baseurl=https://repo.mongodb.org/yum/amazon/2/mongodb-org/4.2/x86_64/
		gpgcheck=1
		enabled=1
		gpgkey=https://www.mongodb.org/static/pgp/server-4.2.asc
--------------------------------------------------------------------------------------
		$ sudo dnf install mongodb-org
		$ sudo systemctl enable mongod.service
		$ sudo systemctl start mongod.service

	https://tecadmin.net/install-mongodb-on-fedora/

### Run app:

$ cd /proyecto-node  
$ npm run dev  
(y en el navegador entrar en el puerto por defecto http://localhost:3000/)  
